package br.com.notificationapp.database.dao;

/**
 * Specific error for database operations
 *
 * @author eduardo-lima
 *
 */
public class DatabaseEx extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DatabaseEx(String message) {
        super(message);
    }
}
