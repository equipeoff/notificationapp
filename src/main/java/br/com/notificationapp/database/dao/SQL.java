package br.com.notificationapp.database.dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.notificationapp.database.models.Notification;

import com.mysql.jdbc.Connection;

/**
 * Contains all necessary interaction with the database
 *
 * @author eduardo-lima
 *
 */
public class SQL {

    private static final String SQL_HOST = "jdbc:mysql://mysql50353-glass.jelasticlw.com.br/notificationapp";
    private static final String SQL_USER = "root";
    private static final String SQL_PASSWORD = "AMKHdGiOgU";//

    private static final String GET_ALL_NOTIFICATIONS = "SELECT id, description, created_at FROM notifications ORDER BY created_at DESC";
    private static final String SAVE_NOTIFICATION = "INSERT INTO notifications (description, created_at) VALUES (?, ?)";

    /**
     * Connect database
     *
     * @return Objeto com as informações de conexão
     * @throws SQLException Exceção SQL
     */
    private static Connection getConnection() throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            throw new SQLException(ex.getMessage());
        }
        return (Connection) DriverManager.getConnection(SQL_HOST, SQL_USER,
                SQL_PASSWORD);
        
//         COMMENT AND UNCOMMENT WHEN Test stage
//        return (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/notificationapp", "root",
//                "");
    }

    /**
     * Get all notifications from db
     *
     * @return List of Notification objects
     * @throws SQLException When an error occurs while sql execution
     */
    public static List<Notification> getNotifications() throws SQLException {
        Connection c = null;
        Statement s = null;
        ResultSet r = null;
        List<Notification> notifications = new ArrayList<>();
        try {
            c = SQL.getConnection();
            s = c.createStatement();
            r = s.executeQuery(SQL.GET_ALL_NOTIFICATIONS);
            while (r.next()) {
                Notification n = new Notification();
                n.setId(r.getInt(1));
                n.setDescription(r.getString(2));
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss");
                    n.setCreatedAt(sdf.parse(r.getString(3)));
                } catch (ParseException e) {
                    System.out.println("erro");
                }
                notifications.add(n);
            }
        } finally {
            c.close();
            s.close();
            r.close();
        }
        return notifications;
    }

    /**
     * Saves the notification into the db
     *
     * @param n Notification Object
     * @throws SQLException When an error occurs while sql execution
     */
    public static void save(Notification n) throws SQLException {

        Connection c = null;
        PreparedStatement stmt = null;
        try {
            c = SQL.getConnection();
            stmt = c.prepareStatement(SQL.SAVE_NOTIFICATION);
            stmt.setString(1, n.getDescription());
            stmt.setString(2, new SimpleDateFormat("yyyy-MM-dd HH:mm").format(n
                    .getCreatedAt()));

            if (stmt.executeUpdate() <= 0) {
                throw new SQLException("Notificação não adicionada!");
            }
        } finally {
            stmt.close();
            c.close();
        }
    }
}
