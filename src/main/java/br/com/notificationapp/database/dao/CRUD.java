package br.com.notificationapp.database.dao;

import java.util.List;

/**
 * Interface for common CRUD operations
 *
 * @author eduardo-lima
 *
 * @param <T> Class models
 */
public interface CRUD<T> {

    public void add(T object) throws DatabaseEx;

    public List<T> getAll() throws DatabaseEx;
}
