package br.com.notificationapp.database.dao;

import java.sql.SQLException;
import java.util.List;

import br.com.notificationapp.database.models.Notification;

/**
 * Class to execute CRUD operations for the models
 *
 * @author eduardo-lima
 *
 */
public class NotificationSQL implements CRUD<Notification> {

    @Override
    public List<Notification> getAll() throws DatabaseEx {
        try {
            return SQL.getNotifications();
        } catch (SQLException e) {
            throw new DatabaseEx(e.getMessage());
        }

    }

    @Override
    public void add(Notification object) throws DatabaseEx {
        try {
            SQL.save(object);
        } catch (SQLException e) {
            throw new DatabaseEx(e.getMessage());
        }
    }

}
