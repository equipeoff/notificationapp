package br.com.notificationapp.database.models;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains id and notification description
 *
 * @author eduardo-lima
 *
 */
@XmlRootElement
public class Notification {

    private int id;
    private String description;
    private Date createdAt; // datetime format for json: "2012-04-23T18:25:43.511Z"

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
