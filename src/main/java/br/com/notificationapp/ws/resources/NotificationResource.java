package br.com.notificationapp.ws.resources;

import br.com.notificationapp.database.dao.CRUD;
import br.com.notificationapp.database.dao.DatabaseEx;
import br.com.notificationapp.database.dao.NotificationSQL;
import br.com.notificationapp.database.models.Notification;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This is a way to other allowed applications apply and change values to the
 * database
 *
 * @author eduardo-lima
 */
@Path("/notification")
public class NotificationResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String respondAsReady() {
        return "Service is ready!";
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Notification addNotification(Notification n) {

        try {
            CRUD<Notification> crud = new NotificationSQL();
            crud.add(n);
        } catch (DatabaseEx ex) {
            Logger.getLogger(NotificationResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }
}
