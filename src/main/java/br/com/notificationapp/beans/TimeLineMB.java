/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.notificationapp.beans;

import br.com.notificationapp.database.dao.CRUD;
import br.com.notificationapp.database.dao.DatabaseEx;
import br.com.notificationapp.database.dao.NotificationSQL;
import br.com.notificationapp.database.models.Notification;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.extensions.model.timeline.TimelineEvent;
import org.primefaces.extensions.model.timeline.TimelineModel;

/**
 * Managed bean for TimeLine widget
 *
 * @author eduardo-lima
 */
@ManagedBean
@ViewScoped
public class TimeLineMB implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private TimelineModel model; // It has all the events in the widget
    private long zoomMax = 1000L * 60 * 60 * 24 * 30 * 3; // 3 months
    private TimeZone timeZone = TimeZone.getTimeZone("America/Sao_Paulo");

    @PostConstruct
    public void init() {

        this.setModel(new TimelineModel());

        try {
            CRUD<Notification> crud = new NotificationSQL();

            List<Notification> ns = crud.getAll();
            for (Notification n : ns) {
                getModel().add(new TimelineEvent(n.getCreatedAt().toString(), n.getCreatedAt()));
            }
        } catch (DatabaseEx e) {
            this.addError(e.getMessage());
        }
    }

    /**
     * Refreshs timeline (poll widget)
     */
    public void refresh() {
        this.getModel().clear();

        try {

            CRUD<Notification> crud = new NotificationSQL();

            List<Notification> ns = crud.getAll();
            for (Notification n : ns) {
                getModel().add(new TimelineEvent(n.getCreatedAt().toString(), n.getCreatedAt()));
            }
        } catch (DatabaseEx e) {
            this.addError(e.getMessage());
        }
    }

    /**
     * Helper to launch error messages
     *
     * @param message
     */
    private void addError(String message) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                message, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the model
     */
    public TimelineModel getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(TimelineModel model) {
        this.model = model;
    }

    /**
     * @return the zoomMax
     */
    public long getZoomMax() {
        return zoomMax;
    }

    /**
     * @param zoomMax the zoomMax to set
     */
    public void setZoomMax(long zoomMax) {
        this.zoomMax = zoomMax;
    }

    /**
     * @return the timeZone
     */
    public TimeZone getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone the timeZone to set
     */
    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

}
