package br.com.notificationapp.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.notificationapp.database.models.Notification;
import br.com.notificationapp.database.dao.DatabaseEx;
import br.com.notificationapp.database.dao.CRUD;
import br.com.notificationapp.database.dao.NotificationSQL;
import java.util.Calendar;

/**
 * Managed bean to list all warnings
 *
 * @author eduardo-lima
 *
 */
@ManagedBean
@ViewScoped
public class NotificationMB implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final List<String> warnings = new ArrayList<>();

    private Date dateTime = new Date();

    @PostConstruct
    public void init() {
        try {
            this.warnings.clear();

            // get today and clear time of day
            Calendar firstHourOfDay = Calendar.getInstance();
            firstHourOfDay.setTime(dateTime);
            firstHourOfDay.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
            firstHourOfDay.clear(Calendar.MINUTE);
            firstHourOfDay.clear(Calendar.SECOND);
            firstHourOfDay.clear(Calendar.MILLISECOND);

            // get last hour
            Calendar lastHourOfDay = Calendar.getInstance();
            lastHourOfDay.setTime(dateTime);
            lastHourOfDay.set(Calendar.HOUR_OF_DAY, 23);
            lastHourOfDay.set(Calendar.MINUTE, 59);
            lastHourOfDay.set(Calendar.SECOND, 59);

            CRUD<Notification> crud = new NotificationSQL();

            // Search for markgings during the day and generate warnigs of time up
            List<Notification> ns = crud.getAll();
            for (Notification n : ns) {
                if ((n.getCreatedAt().after(firstHourOfDay.getTime())) && (n.getCreatedAt().before(lastHourOfDay.getTime()))) {
                    if (n.getCreatedAt().after(this.dateTime)){
                        this.warnings.add("Marcação após horário definido");
                        break;
                    }
                }
            }
        } catch (DatabaseEx e) {
            this.addError(e.getMessage());
        }
    }

    /**
     * Refreshs list of warnings (poll widget)
     */
    public void refresh() {
        this.addInfo("Refresh");
        this.init();

    }

    public void addNotification() {
        Notification n = new Notification();
        n.setDescription("Do botão");
        n.setCreatedAt(new Date());

        try {
            CRUD<Notification> crud = new NotificationSQL();
            crud.add(n);
        } catch (DatabaseEx e) {
            this.addError(e.getMessage());
        }
        this.init();
    }

    public List<String> getWarnings() {
        return this.warnings;
    }

    /**
     * Helper to launch error messages
     *
     * @param message
     */
    private void addError(String message) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                message, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Helper to launch info messages
     *
     * @param message
     */
    private void addInfo(String message) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                message, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the dateTime
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
